public class ThreadState {
    public static Thread thread1;
    public static ThreadState obj;

    public static void main(String[] args) throws InterruptedException {
        //第一种状态切换,新建-运行-停止
//        System.out.println("第一种状态切换");
//        Thread thread1 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("thread1当前状态: " + Thread.currentThread().getState().toString());
//                System.out.println("执行了");
//            }
//        });
//        System.out.println("未调用start方法,当前状态:" + thread1.getState().toString());
//        thread1.start();
//        try {
//            Thread.sleep(2000L); // 等待thread1执行结束，再看状态
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("等待两秒，再看thread1当前状态：" + thread1.getState().toString());


        //第二种：新建 -> 运行 -> 等待 -> 运行 -> 终止(sleep方式)
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("thread2当前状态：" + Thread.currentThread().getState().toString());
                System.out.println("thread2 执行了");
            }
        });
        System.out.println("没调用start方法，thread2当前状态：" + thread2.getState().toString());
        thread2.start();
        System.out.println("调用start方法，thread2当前状态：" + thread2.getState().toString());
        Thread.sleep(2000L); // 等待200毫秒，再看状态
        System.out.println("等待200毫秒，再看thread2当前状态：" + thread2.getState().toString());
        Thread.sleep(3000L); // 再等待3秒，让thread2执行完毕，再看状态
        System.out.println("等待3秒，再看thread2当前状态：" + thread2.getState().toString());
    }
}

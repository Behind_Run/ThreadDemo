public class SynchronizedDemo {
    public static volatile int a = 1;

//    public  static int s = 1;

    public static synchronized void add() {
        a = a + 1;
//        System.out.println("当前线程:" + Thread.currentThread().getName().toString()+ "----" + a );
    }
//    static void alert(){
//        System.out.println(SynchronizedDemo.s);//输出name
//    }

    public static void main(String[] args) {
//        SynchronizedDemo.alert();
        SynchronizedDemo synchronizedDemo = new SynchronizedDemo();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    add();
                    System.out.println("当前线程:" + Thread.currentThread().getName().toString() + "  a的值:" + a + " i:"+i);
                }
            }
        });
        t1.setName("线程1");
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    add();
                    System.out.println("当前线程:" + Thread.currentThread().getName().toString() + "  a的值:" + a + " i:"+i);
                }
            }
        });
        thread.setName("线程2");
        t1.start();
        thread.start();
    }
}
